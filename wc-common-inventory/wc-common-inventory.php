<?php
/*
Plugin Name: Woocommerce Common Inventory
Plugin URI: 
Description: Sends Inventory Updates to Participating Woocommerce Sites. It is a peer to peer system and does not requires a central controller.
Version: 1.5
Author: QualitynAgility
Author URI: http://codecanyon.net/user/QualitynAgility
*/

if ( !class_exists( 'WC_Common_Inventory' )){   
	class WC_Common_Inventory{

		function __construct() {
			add_action( 'admin_menu', array(&$this, 'admin_menu') );
			add_action('admin_enqueue_scripts', array(&$this, 'admin_enqueue_scripts') );
			add_action('init', array(&$this, 'fe_init'));
			add_action( 'wp_ajax_nopriv_qnawccominv', array(&$this, 'peer_req') );
			add_action( 'woocommerce_product_set_stock', array(&$this, 'stock_updated') );
		}
		
		function admin_menu(){
			add_menu_page('Common Inventory', 'Common Inventory', 'manage_options', 'qna_wc_common_inventory', array(&$this, 'common_inventory'),'dashicons-admin-multisite');
			add_submenu_page('qna_wc_common_inventory','Inventory Settings', 'Inventory Settings', 'manage_options', 'qna_wc_common_inv_settings', array(&$this,'settings'));
			
			
		}
		
		function common_inventory(){
			
			?>
				<div class="tw-bs container wci_log_container" >
					<div class="row">
						<div class="col-md-12"><h2 class="wci_log_title">WC Common Inventory Log</h2></div>
						
					</div>
			<?php
			
			$res = $this->get_log();
			if($res){
				$white = true;
				foreach($res as $r){
					
					ob_flush();
					
					if(function_exists('wc_timezone_string')){
						$dt = new DateTime($r->post_date_gmt, new DateTimeZone(wc_timezone_string()));
						$dt= $dt->format('m/d/y H:i:s');
					}else{
						$dt= $r->post_date_gmt;
					}
					
					$ret = unserialize($r->post_content);
					$color = "";
					if($ret['err'] == "Error") $color = " style='color:red; ";
					
					if($white){
						$white_class = "wci_log_white";
						$white = false;
					}else{
						$white_class = "";
						$white = true;
					}
					?>
						
							<div class="row">
								<div class="col-md-12 <?php echo $white_class;?>" <?php echo $color;?>>
									<?php
										echo "$dt ".$r->post_title."<br/>".$ret['msg'];
									?>
								</div>
							</div>
						
					<?php
				}
			}else{
				?>
					<div class="row">
						<div class="col-md-12 wci_not_found">No Log Entries Found</div>
					</div>
				<?php
			}
			
			?>
				</div><!-- container -->
			<?php
			
			$res = $this->purge_log();
			
		}
		
		function get_log(){
			global $wpdb;
			
			$sql = "SELECT * FROM $wpdb->posts where post_type = 'qna_wc_peer_log' order by post_date desc limit 200";
			$res = $wpdb->get_results($sql);
			
			return $res;
			
		}
		
		function purge_log(){
			global $wpdb;
			
			$sql = "Delete FROM $wpdb->posts where post_type = 'qna_wc_peer_log' AND post_date_gmt < '". date("Y-m-d H:i:s", time()-604800)."'";
			$res = $wpdb->query($sql);
			
		}
		
		function stock_updated($prod){
			
			$sites = get_option( '_qna_wc_ci_sites', array());
				
			foreach($sites as $site){
				
				$ret = $this->send_request($site['peer_site'], $site['peer_key'], get_site_url(), $prod->get_sku(), $prod->get_stock_quantity());
				$this->new_log_entry($site['peer_site'], $ret['msg'], true, $ret['status'], $prod->get_sku(),$prod->get_stock_quantity(), $ret['oldqty']);
			}
		}
		
		
		function settings(){
			$this->handle_settings();
			$this->show_settings();
		}
		
		function admin_enqueue_scripts(){
			wp_enqueue_script('jquery');
			wp_enqueue_script('jquery-ui-core');
			wp_enqueue_style( 'tw-bs_bootstrap', plugins_url('css/tw-bs.css', __FILE__) );
			wp_enqueue_style( 'wci', plugins_url('css/wc_common_inventory.css', __FILE__) );
		}
		
		function show_settings(){
			$sites = get_option( '_qna_wc_ci_sites', array());
			
			?>
			<div class="tw-bs container" style="max-width:45em;float:left;background-color: lightgray;margin-top: 2em;">
				<div class="row">
					<div class="col-md-12"><h2 style="text-align:center;">WC Common Inventory Settings</h2></div>
					
				</div>
				<div style="background-color:white;padding: 1em;">
					<div class="row">
						<div class="col-md-12"><b>My Site:</b></div>
					</div>
					<div class="row">
						<div class="col-md-12" style="margin-bottom:1em;"><?php echo get_site_url();?></div>
					</div>
					<div class="row">
						<div class="col-md-12"><b>My Key:</b></div>
					</div>
					<div class="row">
						<div class="col-md-12" style="margin-bottom:1em;"><?php echo $this->get_my_key();?></div>
					</div>
					<div class="row">
						<div class="col-md-12" style="background-color: lightgray;height:5px;margin-bottom:1em;">&nbsp;</div>
					</div>
					
					<div class="row">
						<div class="col-md-12"><b>Peer Sites:</b></div>
					</div>
					<?php
					if(empty($sites)){
					?>
						<div class="row">
							<div class="col-md-12" style="text-align:center;">No Sites Added Yet</div>
						</div>
					<?php
					}else{
						foreach($sites as $site){
					?>
							<div class="row">
								<div class="col-md-6"><?php echo $site['peer_site'];?></div>
								<div class="col-md-5"><?php echo $site['peer_key'];?></div>
								<div class="col-md-1"><a href="<?php echo admin_url( 'admin.php?page=qna_wc_common_inv_settings&peersite='.$site['peer_site'] );?>" onclick="return confirm('Are you sure to delete site?')"><img src="<?php echo plugins_url('img/trash.png', __FILE__); ?>"></a></div>
							</div>
							<div class="row">
								<div class="col-md-12" style="background-color: lightgray;height:1px;margin-top:3px;margin-bottom:3px;">&nbsp;</div>
							</div>
					<?php
						}
					}
					?>
					<div class="row">
						<div class="col-md-12" style="background-color: lightgray;height:5px;margin-bottom:1em;margin-top:1em;">&nbsp;</div>
					</div>
					<div class="row">
						<div class="col-md-12" style="text-align:center;"><b>Add Peer Site</b></div>
					</div>
					<form method="post" action="">
						<div class="form-group">
							<label><b>Peer Site</b>
								<input class="form-control" style="min-width:40em;" type="text" required name="peer_site">
							</label>
						</div>
						<div class="form-group">
							<label><b>Peer Key</b>
								<input class="form-control" style="min-width:40em;" type="text" pattern=".{32}" required name="peer_key">
							</label>
						</div>
						
						<div class="row">
							<div class="col-md-12" style="background:white;padding-bottom:1em;">
								<input type="submit" value="Add Peer Site" name="submit_peer_site" class="btn btn-primary">
							</div>
						</div>
					</form>
				</div>
			</div>
			<?php 
		}
		
		function fe_init(){
			$args = array(
			  'public' => false,
			  'label'  => 'QNA WC INV Peer Log'
			);
			register_post_type( 'qna_wc_peer_log', $args );
		}
		
		function get_my_key(){
			
			$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
			$charactersLength = strlen($characters);
			$randomString = '';
			for ($i = 0; $i < $length; $i++) {
				$randomString .= $characters[rand(0, $charactersLength - 1)];
			}
			
			return md5(admin_url( 'admin-ajax.php?action=qnawccominv' ).$randomString);
		}
		
		function new_log_entry($site, $msg, $sent=false, $err='OK', $sku='',$new=0, $old=0){
			
			$ttl = '';
			if($sent){
				$ttl = "Sent update to $site";
			}else{
				$ttl = "Sent response to $site";
			}
			
			$cnt = array();
			$cnt['err'] = $err;
			$cnt['sku'] = $sku;
			$cnt['old'] = $old;
			$cnt['new'] = $new;
			$cnt['msg'] = $msg;
			
			$my_post = array(
				'post_title'    => $ttl,
				'post_content'  => serialize($cnt),
				'post_status'   => 'publish',
				'post_type'     => 'qna_wc_peer_log'
			);
			
			wp_insert_post($my_post);
			
		}
		
		function peer_req(){
			
			if(isset($_POST['apikey']) && isset($_POST['sku']) && isset($_POST['mysite']) && isset($_POST['newqty'])){
				
				$apikey = $_POST['apikey'];
				$sku 	= $_POST['sku'];
				$mysite = $_POST['mysite'];
				$newqty = $_POST['newqty'];
				
				$sites = get_option( '_qna_wc_ci_sites', array());
				
				foreach($sites as $site){
					
					if($site['peer_site'] == $mysite){
						
						if($this->get_my_key() == $apikey){
							$ret = $this->process_req($sku, $newqty, $mysite);
							
							echo $ret;
							exit;
						}else{
							break;
						}
					}
				}
				
			}
			
			exit;

		}
		
		function create_reply($err, $msg, $site, $oldqty=0, $newqty=0){
			$rep = array();
			
			if($err){
				$rep['status'] = 'Error';
			}else{
				$rep['status'] = 'OK';
			}
			
			$rep['msg'] = $msg;
			$rep['oldqty'] = $oldqty;
			$rep['newqty'] = $newqty;
			
			$this->new_log_entry($site, $msg, false, $rep['status'], '',$newqty, $oldqty);
			
			return serialize($rep);
			
		}
		
		function process_req($sku, $newqty, $site){
			global $wpdb;
			global $woocommerce;
			
			$pid = 0;
			
			if (function_exists('wc_get_product_id_by_sku')) {
				
				$pid = wc_get_product_id_by_sku($sku);
				
			}else{
				
				$pid = $wpdb->get_var( $wpdb->prepare( "SELECT post_id FROM $wpdb->postmeta WHERE meta_key='_sku' AND meta_value='%s' LIMIT 1", $sku ) );
			}
			
			if($pid == 0){
				return $this->create_reply(true, 'Product not found for SKU '.$sku, $site);
			}
			
			$prod = null;
			
			if (function_exists('wc_get_product')) {
				
				$prod = wc_get_product($pid);
				
			}else{
				$prod = new WC_Product($pid);
			}
			
			if(! $prod){
				return $this->create_reply(true, "Product not found for SKU $sku, Product Id $pid", $site);
			}
			
			if(! $prod->managing_stock()){
				return $this->create_reply(true, "Stock is not Enabled for SKU $sku, Product Id $pid", $site);
			}
			
			$qty = $prod->get_stock_quantity();
			
			if($qty == 0){
				return $this->create_reply(true, "Available Stock for SKU $sku, Product Id $pid is already 0", $site);
			}
			
			remove_action( 'woocommerce_product_set_stock', array(&$this, 'stock_updated') );
			$new_stock = $prod->set_stock($newqty);
			add_action( 'woocommerce_product_set_stock', array(&$this, 'stock_updated') );
			
			return $this->create_reply(false, "New Stock for SKU $sku, Product Id $pid is $new_stock, previous stock was $qty", $site, $qty, $new_stock);
			
		}
		
		
		function stub(){
			
		}
		
		function send_request($url, $key, $mysite, $sku, $newqty){
			
			
			$body = array(
						'apikey' => $key,
						'sku' => $sku,
						'mysite' => $mysite,
						'newqty' => $newqty
						);
					
			$args = array(
						'method' => 'POST',
						'timeout' => 45,
						'redirection' => 5,
						'httpversion' => '1.0',
						'blocking' => true,
						'sslverify' => false,
						'headers' => array(),
						'body' => $body,
						'cookies' => array()
						);
						
			$url = $url.'/wp-admin/admin-ajax.php?action=qnawccominv';
			$response = wp_remote_post( $url , $args);
			
			$retarr = array();
			
			if ( is_wp_error( $response ) ) {
			   $error_message = $response->get_error_message();
			   $retarr['status'] = "error";
			   $retarr['err_msg'] = $error_message;
			   
			} else {
				
				$retarr = unserialize($response['body']);
			}
			
			return $retarr;
			
		}
		
		
		function handle_settings(){
			if(isset($_POST['submit_peer_site'])){
				$peer_site = $_POST['peer_site'];
				$peer_key = $_POST['peer_key'];
				
				if(trim(strtolower($peer_site)) == strtolower(get_site_url())){
					echo "<h3 style='color:red;margin:1em;'>New site is the same as this site. Please add PEER site.</h3>";
					return;
				}
				
				$sites = get_option( '_qna_wc_ci_sites', array());
				
				foreach($sites as $site){
					if($site['peer_site'] == trim(strtolower($peer_site))){
						echo "<h3 style='color:red;margin:1em;'>Peer site already exists.</h3>";
						return;
					}
				}
				
				$new_site = array();
				$new_site['peer_site'] = trim(strtolower($peer_site));
				$new_site['peer_key'] = trim(strtolower($peer_key));
				
				$sites[] = $new_site;
				
				update_option( '_qna_wc_ci_sites', $sites );
				
				echo "<h3 style='color:green;margin:1em;'>New site has been added successfully.</h3>";
				
			}elseif(isset($_GET['peersite'])){
				$peersite = $_GET['peersite'];
				
				$sites = get_option( '_qna_wc_ci_sites', array());
				
				$new_sites = array();
				$found = false;
				
				foreach($sites as $site){
					if($site['peer_site'] == trim(strtolower($peersite))){
						$found = true;
						continue;
					}
									
					$new_sites[] = $site;
				}
				
				update_option( '_qna_wc_ci_sites', $new_sites );
				
				if($found) echo "<h3 style='color:green;margin:1em;'>Site has been removed successfully.</h3>";
				
			}
		}
		
		
		
	}//class ends
}//existing class ends

new WC_Common_Inventory();